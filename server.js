var express = require('express')
var dotenv = require('dotenv').config()
const { PORT } = process.env
var app = express()
app.listen(()=>{
    console.log(`Server is listening at port: ${PORT}`)
})