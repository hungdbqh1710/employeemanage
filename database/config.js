//var dotenv = require('dotenv').config()
const { NODE_ENV } = process.env
const {
  [`${NODE_ENV}_DB_USERNAME`]: username,
  [`${NODE_ENV}_DB_PASSWORD`]: password,
  [`${NODE_ENV}_DB_NAME`]: database,
  [`${NODE_ENV}_DB_HOSTNAME`]: host,
  [`${NODE_ENV}_DB_RDBMS`]: dialect
} = process.env

module.exports = {database, username, password, host, dialect}
